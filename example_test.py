import pytest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait


@pytest.fixture
def browser():
    # Set up the WebDriver instance
    driver = webdriver.Chrome()  # You need to have ChromeDriver installed and in PATH
    yield driver
    # Teardown - Quit the WebDriver instance
    driver.quit()


def test_login_logout(browser):
    # Go to the home page
    browser.get("https://practicetestautomation.com/practice-test-login/")
    
    # Verify the home page is displayed correctly
    assert "Example Domain" in browser.title
    
    # Find and click on the "Login" link
    login_link = WebDriverWait(browser, 10).until(
        EC.element_to_be_clickable((By.LINK_TEXT, "Login"))
    )
    login_link.click()
    
    # On the login page, enter incorrect data and click the login button
    username_input = WebDriverWait(browser, 10).until(
        EC.presence_of_element_located((By.ID, "username"))
    )
    password_input = WebDriverWait(browser, 10).until(
        EC.presence_of_element_located((By.ID, "password"))
    )
    login_button = WebDriverWait(browser, 10).until(
        EC.element_to_be_clickable((By.XPATH, "//button[contains(text(), 'Login')]"))
    )
    username_input.send_keys("test")
    password_input.send_keys("test")
    login_button.click()
    
    # Check that an error message is displayed for incorrect login information
    error_message = WebDriverWait(browser, 10).until(
        EC.visibility_of_element_located((By.ID, "error-message"))
    )
    assert error_message.text == "Invalid username or password"
    
    # Enter the correct login information and click the login button
    username_input.clear()
    password_input.clear()
    username_input.send_keys("correct_username")
    password_input.send_keys("correct_password")
    login_button.click()
    
    # Verify successful login and redirection to the home page
    assert "Welcome" in browser.page_source
    
    # Locate and click the Logout link
    logout_link = WebDriverWait(browser, 10).until(
        EC.element_to_be_clickable((By.LINK_TEXT, "Logout"))
    )
    logout_link.click()
    
    # Verify successful logout and redirection to the home page
    assert "Example Domain" in browser.title
